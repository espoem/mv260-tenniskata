namespace Tennis
{
    class TennisGame1 : TennisGame
    {
        public TennisGame1(string player1Name, string player2Name) : base(player1Name, player2Name)
        {
        }

        public override string GetScore()
        {
            if (player1Points == player2Points)
            {
                return this.GetScoreAllSame(player1Points);
            }
            if (player1Points >= 4 || player2Points >= 4)
            {
                return this.GetScoreAboveThreePoints(player1Points, player2Points);
            }

            return $"{SCORES[player1Points]}-{SCORES[player2Points]}";
        }

        private string GetScoreAllSame(int score)
        {
            if (score < 0 || score > 2)
            {
                return DEUCE;
            }

            return $"{SCORES[score]}-All";
        }

        private string GetScoreAboveThreePoints(int player1Score, int player2Score)
        {
            var minusResult = player1Score - player2Score;
            if (minusResult == 1) return $"Advantage {PLAYER1}";
            if (minusResult == -1) return $"Advantage {PLAYER2}";
            if (minusResult >= 2) return $"Win for {PLAYER1}";
            return $"Win for {PLAYER2}";
        }
    }
}