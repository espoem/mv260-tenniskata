﻿namespace Tennis
{
    public abstract class TennisGame : ITennisGame
    {
        public static string[] SCORES = new string[] { "Love", "Fifteen", "Thirty", "Forty" };
        public static string DEUCE = "Deuce";
        protected int player1Points = 0;
        protected int player2Points = 0;
        protected string player1Name;
        protected string player2Name;
        public static string PLAYER1 = "player1";
        public static string PLAYER2 = "player2";

        protected TennisGame(string player1Name, string player2Name)
        {
            this.player1Name = player1Name;
            this.player2Name = player2Name;
        }

        public abstract string GetScore();

        public virtual void WonPoint(string playerName) {
            if (playerName == PLAYER1)
                player1Points++;
            else
                player2Points++;
        }
    }
}