namespace Tennis
{
    public class TennisGame2 : TennisGame
    {
        public TennisGame2(string player1Name, string player2Name) : base(player1Name, player2Name)
        {
        }

        public override string GetScore()
        {
            if (player1Points == player2Points)
            {
                return player1Points < 3 ? $"{SCORES[player1Points]}-All" : DEUCE;
            }

            var score = "";
            TryGetScoreLovePlayer(player1Points, player2Points, ref score);
            TryGetScoreLeadingPlayer(player1Points, player2Points, ref score);
            TryGetScoreAdvantage(player1Points, player2Points, ref score);
            TryGetScoreWin(player1Points, player2Points, ref score);
            return score;
        }

        private void TryGetScoreWin(int points1, int points2, ref string score)
        {
            if (IsPlayerWinner(points1, points2))
            {
                score = $"Win for {PLAYER1}";
            }
            if (IsPlayerWinner(points2, points1))
            {
                score = $"Win for {PLAYER2}";
            }
        }

        private void TryGetScoreAdvantage(int points1, int points2, ref string score)
        {
            if (HasPlayerAdvantage(points1, points2))
            {
                score = $"Advantage {PLAYER1}";
            }

            if (HasPlayerAdvantage(points2, points1))
            {
                score = $"Advantage {PLAYER2}";
            }
        }

        private void TryGetScoreLovePlayer(int points1, int points2, ref string score)
        {
            if (points1 > 0 && points2 == 0)
            {
                string s1 = points1 <= 3 ? SCORES[points1] : "";
                score = $"{s1}-{SCORES[0]}";
            }
            if (points2 > 0 && points1 == 0)
            {
                string s2 = points2 <= 3 ? SCORES[points2] : "";
                score = $"{SCORES[0]}-{s2}";
            }
        }

        private void TryGetScoreLeadingPlayer(int points1, int points2, ref string score)
        {
            if (points1 > points2 && points1 < 4 || points1 < points2 && points2 < 4)
            {
                score = $"{SCORES[points1]}-{SCORES[points2]}";
            }
        }

        private bool IsPlayerWinner(int playerScore, int otherScore)
        {
            return playerScore >= 4 && otherScore >= 0 && (playerScore - otherScore) >= 2;
        }

        private bool HasPlayerAdvantage(int playerScore, int otherScore)
        {
            return playerScore > otherScore && otherScore >= 3;
        }
    }
}