namespace Tennis
{
    public class TennisGame3 : TennisGame
    {

        public TennisGame3(string player1Name, string player2Name) : base(player1Name, player2Name)
        {
        }

        public override string GetScore()
        {
            if (player1Points < 4 && player2Points < 4 && (player1Points + player2Points < 6))
            {
                return GetScoreBelowFourPoints();
            }

            if (player1Points == player2Points)
                return DEUCE;

            return GetScoreWinning();

        }

        private string GetScoreWinning()
        {
            string winningPlayer = player1Points > player2Points ? player1Name : player2Name;
            int scoreDiff = player1Points - player2Points;
            bool hasAdvantage = scoreDiff * scoreDiff == 1;
            return hasAdvantage ? $"Advantage {winningPlayer}" : $"Win for {winningPlayer}";
        }

        private string GetScoreBelowFourPoints() => (player1Points == player2Points) ? $"{SCORES[player1Points]}-All" : $"{SCORES[player1Points]}-{SCORES[player2Points]}";
    }
}

